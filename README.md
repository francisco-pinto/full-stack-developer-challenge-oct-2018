Introduction
============

This document will guide you through the tasks we expect you to complete.


Challenge
=========

The goal is to create a simple web application that allows users to navigate a social graph and open people's profiles.

Every time a new profile is requested, the front-end application submits a request over a RESTful API to the server on the back-end. The server will then fetch the requested data from a database and submit the results back to the front-end.

The data is then dynamically rendered on the front-end using HTML, CSS, and/or JavaScript.

A diagram of the application is shown bellow.

![](http://cedegemac7.epfl.ch/files/GraphChallenge.png)


Back-end
--------

The framework that interacts with the database and provides a RESTful API should be programmed in DJango.

You may use any database of your choice (MySQL, Postgres, Mongo, etc). You may also fill in the database manually, either with the data shown in the diagram above or any other dataset of your choice. Since the back-end will only be dealing with GET requests, the database can be static.

We have provided a docker compose file and set up a container with python3 and django for you to complete the task.


Front-end
---------

The web application should allow users to navigate the social graph by:

- clicking the nodes directly on the graph
- clicking the links under "Related people"

This implies that the graph nodes should be clickable.
Everytime a profile is opened, the respective node is highlighted.

You may use any JavaScript library of your choice to design the interactive graph (e.g., D3, sigmaJS, jQuery).


Assessment
==========

We are looking for someone not only with experience but also self-motivated and able to develop creative solutions. 
We welcome thinking out of the box! This assessment is designed to challenge you, and provide an instructive and fun experience.

Extra kudos if you document your solution, either with comments on your code or in a separate documents' structure. 
For example, you can use sphinx to document relevant development decisions, commands, research, web links etc.

We like people who are able to learn and research. Even if you might not get the tasks completed, your notes will help us in the evaluation of your work.


Other notes
==========

- Please try and complete the assessment within one week of receiving it.
- Explain your solutions in 2-3 lines.
- Create git repository to track and share your work. Please link commits relevant to each task completed. We recommend you create a Bitbucket account and a private git repository (there's no cost to you). Invite "francisco-pinto" and "odemakov" to your repository team.
- Share your results even if you don't finish.
- We understand there's more than one way to do things. If you're stuck, pick one solution and justify your choice.


Assumptions
===========

You are proficient in DJango and JavaScript.

You know how to use docker. If you don't, a good starting point is https://docs.docker.com/get-started/.
We avoid doing any manual work when setting up or deploying services. Your solution should run by simply running `docker-compose up`.


Rules
=====

Please keep this assessment to yourself, i.e. don't share it with your colleagues, friends, or anyone else.
Keep the repository private.

You, and you alone, are expected to be the unique author of the solutions for this assessment. 
You will be questioned on details of the solution you provide.


Feedback
========

Please feel free to provide any feedback you have about this assessment, or propose any technical corrections.
